import { combineReducers } from "redux";
import phoneBookReducer from '../slices/phone-book';

export default combineReducers({
  phoneBook: phoneBookReducer,
});
