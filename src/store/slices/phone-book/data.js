const contacts = {
  '2a8146d1-39de-446c-9f62-ea1f92a3e643': {
    name: 'Yadira',
    phoneNumber: '262.555.0804',
  },
  '3393c779-a0be-480b-b761-03fd6e4569f1': {
    name: 'Jaydon',
    phoneNumber: '513.282.7915',
  },
  '915567f6-a742-4fdd-b974-9e3495b93e97': {
    name: 'Dane',
    phoneNumber: '(466) 856-6414',
  },
  '128a602d-688c-460c-9f93-0f251a29005f': {
    name: 'Hollie',
    phoneNumber: '285-379-8288 x9551',
  },
  'a9abbe25-180e-4f29-b3ed-edaec71fed8e': {
    name: 'Michel',
    phoneNumber: '1-225-622-0198',
  },
  'e313ab63-e30b-41d4-b852-dd24be3e2ada': {
    name: 'Dax',
    phoneNumber: '1-363-363-8784 x73084',
  },
  '684b8a10-2ce4-4b49-a7ed-e1cabce4dc49': {
    name: 'Clemens',
    phoneNumber: '638-378-9393',
  },
};

export default contacts;
