import reducer, {
  initialState,
  addEmptyContact,
  deleteContact,
  modifyContact,
  selectContacts,
  selectContact,
} from './index';

it('add empty contact', () => {
  expect(Object.keys(initialState.contacts)).toHaveLength(7);

  const nextState = reducer(initialState, addEmptyContact());

  expect(Object.keys(nextState.contacts)).toHaveLength(8);
});

describe('when there is already an empty contact', () => {
  it('does not add a new one', () => {
    expect(Object.keys(initialState.contacts)).toHaveLength(7);

    const nextState = reducer(initialState, addEmptyContact());
  
    expect(Object.keys(nextState.contacts)).toHaveLength(8);
  
    const nextState2 = reducer(nextState, addEmptyContact());
  
    expect(Object.keys(nextState2.contacts)).toHaveLength(8);
  });
});


it('delete contact', () => {
  const id = '2a8146d1-39de-446c-9f62-ea1f92a3e643';
  const nextState = reducer(initialState, deleteContact({
    id,
  }));

  expect(selectContacts({ phoneBook: nextState })).toEqual({
    ...initialState.contacts,
    [id]: undefined,
  });

  expect(selectContact({ phoneBook: nextState }, id)).toEqual(undefined);
});

it('modify contact', () => {
  const id = '3393c779-a0be-480b-b761-03fd6e4569f1';
  const data = {
    name: 'Devin',
    phoneNumber: '372.845.9230',
  };
  const nextState = reducer(initialState, modifyContact({
    id,
    data,
  }));

  expect(selectContacts({ phoneBook: nextState })).toEqual({
    ...initialState.contacts,
    [id]: { ...data }
  });

  expect(selectContact({ phoneBook: nextState }, id)).toEqual({
    ...data,
  });
});