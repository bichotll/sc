import {
  createSlice,
} from '@reduxjs/toolkit';
import { v4 as uuid } from 'uuid';

// for the test's sake
import contactsData from './data';

export const initialState = {
  contacts: {
    ...contactsData,
  },
};

const phoneBookSlice = createSlice({
  name: 'phoneBook',
  initialState: {
    ...initialState,
  },
  // redux toolkit uses immer under the hook - this wacky magic inside the reducers will work :)
  reducers: {
    addEmptyContact(state) {
      if (Object.values(state.contacts).find(({ name, phoneNumber }) => name === '' && phoneNumber === '')) {
        return;
      }

      state.contacts[uuid()] = {
        name: '',
        phoneNumber: '',
      };
    },
    deleteContact(state, action) {
      delete state.contacts[action.payload.id];
    },
    modifyContact(state, action) {
      state.contacts[action.payload.id] = action.payload.data;
    },
  },
});

export const {
  addContact,
  addEmptyContact,
  deleteContact,
  modifyContact,
} = phoneBookSlice.actions;

export const selectContacts = state => state.phoneBook.contacts;
export const selectContact = (state, contactId) => state.phoneBook.contacts[contactId];

export default phoneBookSlice.reducer;
