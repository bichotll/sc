import { Provider } from 'react-redux';
import store from './store';
import PhoneBookTable from './ui/PhoneBookTable/container';
import 'antd/dist/antd.css';

function App() {
  return (
    <Provider store={store}>
      <PhoneBookTable />
    </Provider>
  );
}

export default App;
