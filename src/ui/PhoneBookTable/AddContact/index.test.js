import { screen , render } from '@testing-library/react'
import Component from './index';

it('calls onClick when click button', () => {
  const handleOnClick = jest.fn();
  render(
    <Component
      onClick={handleOnClick}
    />
  );

  screen.getByRole('button').click();
  expect(handleOnClick).toHaveBeenCalled();
});
