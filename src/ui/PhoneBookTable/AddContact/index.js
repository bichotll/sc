import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd';

const AddContact = ({ onClick }) => (
  <Button
    onClick={onClick}
    role='button'
  >
    Add Contact
  </Button>
);

AddContact.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default AddContact;
