import { screen , render, fireEvent, act } from '@testing-library/react'
import Component from './index';
import contactsData from '../../../store/slices/phone-book/data';

// matchMedia is not defined in JSDom
// should be moved somewhere else more global
window.matchMedia = window.matchMedia || function() {
  return {
      matches: false,
      addListener: function() {},
      removeListener: function() {}
  };
};

let rows = []

beforeEach(() => {
  rows = Object.keys(contactsData).map(key => ({
    key,
    ...contactsData[key],
  }))
});

const defaultProps = {
  onModifyRow: () => {},
  onDeleteRow: () => {},
};

const editRegex = /Edit/;
const deleteRegex = /Delete/;
const saveRegex = /Save/;
const cancelRegex = /Cancel/;

it('displays rows with all information + edit/delete buttons', () => {
  render(
    <Component rows={rows} {...defaultProps} />
  );

  rows.forEach((row) => {
    expect(screen.getByText(row.name)).toBeInTheDocument();
    expect(screen.getByText(row.phoneNumber)).toBeInTheDocument();
  });

  expect(screen.getAllByText(editRegex)).toHaveLength(rows.length);
  expect(screen.getAllByText(deleteRegex)).toHaveLength(rows.length);
})

describe('when clicking Delete of a row', () => {
  it('calls onDeleteRow and does not delete row', () => {
    const onDeleteRow = jest.fn();
    render(
      <Component
        rows={rows}
        {...defaultProps}
        onDeleteRow={onDeleteRow}
      />
    );

    screen.getAllByText(deleteRegex)[0].click();

    const firstRow = rows[0];
    const thirdRow = rows[2];

    expect(screen.getByText(firstRow.name)).toBeInTheDocument();
    expect(screen.getByText(firstRow.phoneNumber)).toBeInTheDocument();
    
    expect(onDeleteRow).toHaveBeenCalledWith(firstRow.key);

    expect(screen.getByText(firstRow.name)).toBeInTheDocument();
    expect(screen.getByText(firstRow.phoneNumber)).toBeInTheDocument();

    onDeleteRow.mockReset();

    screen.getAllByText(deleteRegex)[2].click();

    expect(onDeleteRow).toHaveBeenCalledWith(thirdRow.key);

    expect(screen.getByText(thirdRow.name)).toBeInTheDocument();
    expect(screen.getByText(thirdRow.phoneNumber)).toBeInTheDocument();
  });
});

describe('on clicking Edit', () => {
  const newName = 'Oral';
  const newPhoneNumber = '971-959-2980';
  const onModifyRow = jest.fn();

  beforeEach(() => {
    render(
      <Component
        rows={rows}
        {...defaultProps}
        onModifyRow={onModifyRow}
      />
    );

    screen.getAllByText(editRegex)[3].click();
  })

  it('disables all the other Edit Delete buttons from the other rows', () => {
    screen.getAllByText(editRegex).forEach((button) => {
      expect(button).toHaveClass('ant-typography-disabled');
    });
    screen.getAllByText(deleteRegex).forEach((button) => {
      expect(button).toHaveClass('ant-typography-disabled');
    });
  });

  it('changes the Edit button by two new ones Save and Cancel', () => {
    expect(screen.getAllByText(saveRegex)).toHaveLength(1);
    expect(screen.getAllByText(cancelRegex)).toHaveLength(1);

    expect(screen.getAllByText(editRegex)).toHaveLength(rows.length - 1);
    expect(screen.getAllByText(deleteRegex)).toHaveLength(rows.length - 1);
  });

  it('changes rows by inputs', () => {
    expect(screen.getByTestId('input-name')).toHaveValue(rows[3].name);
    expect(screen.getByTestId('input-phoneNumber')).toHaveValue(rows[3].phoneNumber);
  });

  describe('when editing', () => {
    beforeEach(() => {
      fireEvent.input(screen.getByTestId('input-name'), {
        target: { value: newName }
      });
      fireEvent.input(screen.getByTestId('input-phoneNumber'), {
        target: { value: newPhoneNumber }
      });

      onModifyRow.mockReset();
    });

    describe('and then clicking cancel', () => {
      beforeEach(() => {
        screen.getByText(cancelRegex).click();
      });

      it('does not save the info neither call onModifyRow', () => {
        expect(screen.queryByText(newName)).not.toBeInTheDocument();
        expect(screen.queryByText(newPhoneNumber)).not.toBeInTheDocument();

        expect(screen.queryByText(saveRegex)).not.toBeInTheDocument();
        expect(screen.queryByText(cancelRegex)).not.toBeInTheDocument();

        expect(onModifyRow).not.toHaveBeenCalled();
      });
    });

    describe('and then clicking save', () => {
      beforeEach(() => {
        screen.getByText(saveRegex).click();
      });

      it('does not save the info and calls onModifyRow', () => {
        expect(screen.queryByText(newName)).not.toBeInTheDocument();
        expect(screen.queryByText(newPhoneNumber)).not.toBeInTheDocument();

        expect(screen.queryByText(saveRegex)).not.toBeInTheDocument();
        expect(screen.queryByText(cancelRegex)).not.toBeInTheDocument();

        expect(onModifyRow).toHaveBeenCalledWith(rows[3].key, {
          name: newName,
          phoneNumber: newPhoneNumber,
        });
      });
    });
  });
});
