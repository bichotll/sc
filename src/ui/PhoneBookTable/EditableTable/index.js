import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Table, Input, Form, Typography } from 'antd';

const EditableCell = ({
  isEditing,
  name,
  dataIndex,
  title,
  phoneNumber,
  children,
  ...restProps
}) => {
  return (
    <td {...restProps}>
      {isEditing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Please input ${title}`,
            }
          ]}
        >
          <Input data-testid={`input-${title}`} />
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

EditableCell.propTypes = {
  isEditing: PropTypes.bool,
  name: PropTypes.string,
  dataIndex: PropTypes.string,
  title: PropTypes.string,
  phoneNumber: PropTypes.string,
};

const EditableTable = ({ rows, onDeleteRow, onModifyRow }) => {
  const [form] = Form.useForm();
  const [editingKey, setEditingKey] = useState('');

  const isEditing = (record) => record.key === editingKey;

  const edit = (record) => {
    form.setFieldsValue({ name: '', phoneNumber: '', ...record });
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key) => {
    try {
      const row = (await form.validateFields());

      onModifyRow(key, row);
      setEditingKey('');
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const columns = [
    {
      title: 'name',
      dataIndex: 'name',
      width: '40%',
      editable: true,
    },
    {
      title: 'phoneNumber',
      dataIndex: 'phoneNumber',
      width: '40%',
      editable: true,
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)}
              style={{ marginRight: 8 }}
              role='button'
            >
              Save
            </Typography.Link>
            <Typography.Link
              onClick={cancel}
              role='button'
            >
              Cancel
            </Typography.Link>
          </span>
        ) : (
          <span>
            <Typography.Link
              disabled={editingKey !== ''}
              onClick={() => edit(record)}
              style={{ marginRight: 8 }}
              role='button'
            >
              Edit
            </Typography.Link>
            <Typography.Link
              disabled={editingKey !== ''}
              onClick={() => onDeleteRow(record.key)}
              role='button'
            >
              Delete
            </Typography.Link>
          </span>
        );
      },
    },
  ];

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        isEditing: isEditing(record),
      }),
    };
  });

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={rows}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={false}
      />
    </Form>
  );
};

EditableTable.propTypes = {
  rows: PropTypes.array.isRequired,
  onDeleteRow: PropTypes.func.isRequired,
  onModifyRow: PropTypes.func.isRequired,
};

export default EditableTable;
