import { connect } from 'react-redux';
import Component from './index';

import {
  addEmptyContact,
  deleteContact,
  modifyContact,
  selectContacts
} from '../../store/slices/phone-book';

const mapStateToProps = state => ({
  contacts: selectContacts(state),
});

const mapDispatchToProps = {
  onAddEmptyContact: () => addEmptyContact(),
  onDeleteContact: (payload) => deleteContact(payload),
  onModifyContact: (payload) => modifyContact(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
