import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';
import AddContact from './AddContact';
import EditableTable from './EditableTable';

const Container = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 50px;
`;

const EditableTableContainer = styled.div`
  width: 600px;
`;

export default function PhoneBookTable({
  onAddEmptyContact,
  onDeleteContact,
  onModifyContact,
  contacts,
}) {
  const rows = Object.keys(contacts).map(key => ({
    key,
    ...contacts[key],
  }))

  return (
    <Container>
      <EditableTableContainer>
        <EditableTable
          rows={rows}
          onDeleteRow={(key) => {
            onDeleteContact({ id: key });
          }}
          onModifyRow={(key, data) => {
            onModifyContact({
              id: key,
              data,
            });
          }}
        />
        <br />
        <AddContact onClick={onAddEmptyContact} />
      </EditableTableContainer>
    </Container>
  )
}

EditableTable.propTypes = {
  onAddEmptyContact: PropTypes.func,
  onDeleteContact: PropTypes.func,
  onModifyContact: PropTypes.func,
  contacts: PropTypes.object,
};
